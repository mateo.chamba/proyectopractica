
package controlador.util;


import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.NodoLista;
import modelo.Sucursal;
import modelo.Venta;


/**
 *
 * @author mateochamba
 */
public class Utilidades {

    public static Double sumarVenta(Sucursal s) {
        double suma = 0.0;
        ListaEnlazada<Venta> listaVentas = s.getVentas();
        
        NodoLista<Venta> nodo = listaVentas.getCabeceraSucursal();
        while(nodo != null){
            Venta venta = nodo.getInfo();
            suma += venta.getValor();
            nodo = nodo.getSiguiente();
        }
        return suma;
    }

    public static Double mediaVentas(Sucursal s) {
        Double suma = sumarVenta(s);
        ListaEnlazada<Venta> listaVentas = s.getVentas();
        
        if (suma == 0) {
            return suma;
        } else {
            return suma / listaVentas.size();
        }

    }
}

