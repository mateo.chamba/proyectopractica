
package controlador.ed.pila;

import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.TopeException;
import controlador.ed.lista.exception.VacioException;

/**
 *
 * @author mateochamba
 */
public class PilaI <E> extends ListaEnlazada<E>{
    private Integer cima;

    public PilaI(Integer cima) {
        this.cima=cima;
    }
    
    public Boolean isFull(){
        
        return(size()>= cima);
    }
    
    public void push(E info) throws TopeException{
        if (!isFull()){
            insertarPrincipal(info);
        }else{
            throw new TopeException();
        }
    }
    
    public E pop() throws VacioException, PosicionException{
        E dato = null;
        if (estaVacio()){
            throw new VacioException("  pila vacia");
        }else{
            return this.borrar(0);
        }
    }
    public Integer getCima(){
        return cima;
    }
    
}
