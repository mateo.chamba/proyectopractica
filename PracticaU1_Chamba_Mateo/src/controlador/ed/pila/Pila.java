
package controlador.ed.pila;

import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.TopeException;
import controlador.ed.lista.exception.VacioException;

/**
 *
 * @author mateochamba
 */
public class Pila <E> {
    private PilaI<E> pilai;

    public Pila(Integer cima) {
        pilai = new PilaI<>(cima);
    }
    
    
    public void push(E obj) throws TopeException{
        pilai.push(obj);
    }
    
    public E pop() throws VacioException, PosicionException{
        return pilai.pop();
    }
    
    public Integer getCima(){
        return pilai.getCima();
    }
    public void print()throws VacioException{
        pilai.imprimir();
    }
}
