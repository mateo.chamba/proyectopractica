
package controlador.ed.lista;

import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import modelo.Sucursal;


/**
 *
 * @author mateochamba
 */
public class ListaEnlazada<E> {

    private NodoLista<E>  cabeceraSucursal;
    private Integer size = 0; 
    
    
  
  
    public void insertar (E info){
        NodoLista<E> valorNuevo = new NodoLista<>(info, null);
        if (estaVacio()){
            this.cabeceraSucursal = valorNuevo;
            this.size++;
        }else {
            NodoLista<E> aux = cabeceraSucursal;
            
            while(aux.getSiguiente()!= null){
                aux = aux.getSiguiente();
            }
            aux.setSiguiente(valorNuevo);
            this.size++;
        }
        
    }
    
    public void insertarPrincipal(E info){
        if(estaVacio()){
            insertar(info);
        }else{
            NodoLista<E> valorNuevo = new NodoLista<>(info, null);
            valorNuevo.setSiguiente(cabeceraSucursal);
            cabeceraSucursal = valorNuevo;
            size++;
        }
    }
    
    public Boolean estaVacio(){
        return cabeceraSucursal == null;
    }
    
    public Integer size(){
        return size;
    }
    public void borrarTodo(){
        this.cabeceraSucursal = null;
    }

    public void insertarPosicion(E info, Integer pos) throws PosicionException {
        if (estaVacio()){
            insertar(info);
            
        } else if(pos==0){
            insertarPrincipal(info);
            
        } else if (pos > 0 && pos < size()){
            NodoLista<E> valorNuevo = new NodoLista<>(info, null);
            NodoLista<E> aux = cabeceraSucursal;
            
            for (int i = 0; i < (pos -1); i++){
                aux = aux.getSiguiente();
            }
            NodoLista<E> siguiente = aux.getSiguiente();
            aux.setSiguiente(valorNuevo);
            valorNuevo.setSiguiente(siguiente);
            size++;
        } else {
            throw new PosicionException();
        }
    }
    
    public E get(Integer pos) throws VacioException, PosicionException{
        if (estaVacio()){
            throw new VacioException();
            
        }else {
            E dato = null;
            if (pos >= 0 && pos < size()){
                if (pos == 0){
                    dato = cabeceraSucursal.getInfo();
                } else {
                    NodoLista<E> aux = cabeceraSucursal;
                    for(int i=0; i<(pos); i++){
                        aux = aux.getSiguiente();
                    }
                    dato = aux.getInfo();
                }
                
            }else {
                throw new PosicionException();
            }
            return dato;
        }
        
        
    }
    
      public void modificar(E info, Integer pos) throws PosicionException{
          NodoLista<E> aux = cabeceraSucursal;
          int indice = 0;
          
          while (aux != null){
              if (indice == pos){
                  aux.setInfo(info);
                  break;
              }
              aux = aux.getSiguiente();
              indice++;
          }
   
    }
    
    public E borrar (Integer pos) throws VacioException, PosicionException{
        
        if (estaVacio()){
            throw new VacioException();
            
        }else{
            E dato = null;
            if (pos >= 0 && pos < size()){
                if(pos == 0){
                    dato = cabeceraSucursal.getInfo();
                    cabeceraSucursal = cabeceraSucursal.getSiguiente();
                    size--;
                } else {
                    NodoLista<E> aux = cabeceraSucursal;
                    for (int i =0; i < ( pos-1); i++){
                        aux = aux.getSiguiente();
                    }
                    NodoLista<E> aux1 = aux.getSiguiente();
                    dato = aux1.getInfo();
                    
                    NodoLista<E> prox = aux.getSiguiente();
                    aux.setSiguiente(prox.getSiguiente());
                    size --;
                }
            }else{
                throw new PosicionException();
            }
                return dato;
        }
        
    }
 
    public void imprimir() throws VacioException {
        if (estaVacio()) {
            throw new VacioException();
        } else {
            NodoLista<E> aux = cabeceraSucursal;
            System.out.println("------------LIsta de sucursal------------");

            for (int i = 0; i < size(); i++) {
                System.out.println(aux.getInfo());
                aux = aux.getSiguiente();
            }

            System.out.println("--------------");
            System.out.println("------- FIn de la lista-----------");
        }
    }
    


    public NodoLista getCabeceraSucursal() {
        return cabeceraSucursal;
    }

    public void setCabeceraSucursal(NodoLista cabeceraSucursal) {
        this.cabeceraSucursal = cabeceraSucursal;
    }
}   
