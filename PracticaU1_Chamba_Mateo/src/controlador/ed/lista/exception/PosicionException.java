
package controlador.ed.lista.exception;

/**
 *
 * @author mateochamba
 */
public class PosicionException extends Exception{

    public PosicionException() {
        super("No hay posicion");
    }
    
    public PosicionException(String mensaje){
        super(mensaje );
    }
    
}
