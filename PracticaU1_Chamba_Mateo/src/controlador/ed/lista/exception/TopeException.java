/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.ed.lista.exception;

/**
 *
 * @author mateochamba
 */
public class TopeException extends Exception{

    public TopeException() {
        super("no hay mas espacion el la cola");
    }
    
    public TopeException(String mensaje){
        super(mensaje);
    }
    
}
