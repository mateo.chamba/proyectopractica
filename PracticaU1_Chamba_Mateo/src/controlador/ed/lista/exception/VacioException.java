
package controlador.ed.lista.exception;

/**
 *
 * @author mateochamba
 */
public class VacioException extends Exception{

    public VacioException() {
        super("Lista vacia");
    }
    public VacioException(String mensaje){
        super(mensaje);
    }
    
    
}
