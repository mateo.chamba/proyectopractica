
package controlador.ed.lista;

/**
 *
 * @author mateochamba
 */
public class NodoLista<E> {
    private E info; 
    private NodoLista <E>siguiente;

    public NodoLista() {
        info = null;
        siguiente = null; 
    }

    public NodoLista(E info, NodoLista siguiente) {
        this.info = info;
        this.siguiente = siguiente;
    }
    
    public E getInfo(){
        return info;
    }
    public void setInfo(E info) {
        this.info = info;
    }

    public NodoLista<E> getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(NodoLista<E> siguiente) {
        this.siguiente = siguiente;
    }

  
    
}
