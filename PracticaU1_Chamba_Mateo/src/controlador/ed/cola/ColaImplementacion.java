
package controlador.ed.cola;

import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.TopeException;
import controlador.ed.lista.exception.VacioException;


/**
 *
 * @author mateochamba
 */
public class ColaImplementacion <E> extends ListaEnlazada<E>{
    private Integer tope; 

    public ColaImplementacion() {
    }

    public ColaImplementacion(Integer tope) {
        this.tope = tope;
    }
    
    public Boolean isFull(){
        return (size()>=tope);
    }
    
    public void queue(E dato )throws TopeException{
        if ( isFull())
            throw new TopeException("Sin espacio");
        else 
            this.insertar(dato);
    }
    
    public E dequeue() throws VacioException, PosicionException{
        if (estaVacio()){
            throw new VacioException("Cola se encuentra vacia");
        }
     else {
            return this.borrar(0);
    }
    
}

    public Integer getTope() {
        return tope;
    }

    public void setTope(Integer tope) {
        this.tope = tope;
    }
}
