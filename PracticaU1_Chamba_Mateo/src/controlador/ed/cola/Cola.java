
package controlador.ed.cola;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.TopeException;
import controlador.ed.lista.exception.VacioException;


/**
 *
 * @author mateochamba
 */
public class Cola<E> {
    private ColaImplementacion<E> cola;
    
    public Cola(Integer tope) {
        cola = new ColaImplementacion<>(tope);
    }
   
    public void queue(E obj) throws TopeException{
        cola.queue(obj);
    }
    
    public E dequeue() throws VacioException, PosicionException{
        return  cola.dequeue();
    }
 
    public Integer getTope(){
        return cola.getTope();
    }
    
    public void print() throws VacioException{
        cola.imprimir();
    }

    public ColaImplementacion<E> getCola() {
        return cola;
    }

    public void setCola(ColaImplementacion<E> cola) {
        this.cola = cola;
    }
    
    

 
 
    
}
