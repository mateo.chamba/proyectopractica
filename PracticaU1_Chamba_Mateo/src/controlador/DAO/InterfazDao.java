
package controlador.DAO;

import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import java.io.IOException;

/**
 *
 * @author mateochamba
 */
public interface InterfazDao<T> {
    public void guardar(T obj) throws IOException;
    public void modificar(T obj, Integer pos) ;
    public ListaEnlazada<T> listar();
    public T obtener (Integer id);
}
