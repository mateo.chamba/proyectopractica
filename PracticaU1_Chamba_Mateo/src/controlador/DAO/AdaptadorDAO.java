
package controlador.DAO;

import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author mateochamba
 */
public class AdaptadorDAO <T> implements InterfazDao<T>{
    
    private Conexion conexion;
    private Class clazz;
    private String url;

    public AdaptadorDAO(Class clazz) {
        this.conexion = new Conexion();
        this.clazz = clazz;
        this.url = Conexion.URL + clazz.getSimpleName().toLowerCase() + ".json";
    }
    
    @Override
    public void guardar(T obj) {
        
        try {
            ListaEnlazada<T> lista = listar();
            lista.insertar(obj);
            conexion.getXstream().alias(lista.getClass().getName(), ListaEnlazada.class);
            conexion.getXstream().toXML(lista, new FileWriter(url));
       
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
            
      
    }

    @Override
    public ListaEnlazada<T> listar(){
        ListaEnlazada<T> lista = new ListaEnlazada<>();
        
        try {
            lista = (ListaEnlazada<T>) conexion.getXstream().fromXML(new File(url));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return lista;
    }
    
    
   @Override
   public void modificar(T obj, Integer pos) {
       ListaEnlazada<T> lista = listar();
       try {
           lista.modificar(obj,pos);

           conexion.getXstream().alias(lista.getClass().getName(), ListaEnlazada.class);
           conexion.getXstream().toXML(lista, new FileWriter(url));
       } catch (PosicionException | IOException ex) {
           System.out.println(ex.getMessage());
       }

        
   }

    
    @Override
    public T obtener(Integer id) {
        ListaEnlazada<T> lista = listar();
        return(T) lista;
    }
    
    
    public Integer geberarId(){
        return listar().size()+1;
    }
}
    

