
package controlador.DAO;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;
import com.thoughtworks.xstream.security.AnyTypePermission;

/**
 *
 * @author mateochamba
 */
public class Conexion {
    
    private XStream xstream;
    public static  String URL = "data/";

    public Conexion() {
        concetar();
    }
    
    public XStream concetar(){
        xstream= new XStream(new JettisonMappedXmlDriver());
        xstream.setMode(XStream.NO_REFERENCES);
        xstream.addPermission(AnyTypePermission.ANY);
        return xstream;
    }

    public XStream getXstream() {
        if(xstream== null){
            concetar();
        }
        return xstream;
    }

  
    
    
}
