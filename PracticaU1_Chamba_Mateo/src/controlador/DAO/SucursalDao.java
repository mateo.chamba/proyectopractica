
//package controlador.DAO;
//
//import controlado.Exception.EspacioException;
//import controlador.ed.lista.exception.PosicionException;
//import controlador.ed.lista.exception.VacioException;
//import java.io.IOException;
//import modelo.EnumMes;
//import modelo.Sucursal;
//import modelo.Venta;
//
///**
// *
// * @author mateochamba
// */
//public class SucursalDao extends AdaptadorDAO<Sucursal>{
//    private Sucursal sucursal;
//
//    public SucursalDao() {
//        super(Sucursal.class);
//    }
//
//    public Sucursal getSucursal() {
//        if (this.sucursal == null){
//            this.sucursal = new Sucursal();
//        }
//        return sucursal;
//    }
//
//    public void setSucursal(Sucursal sucursal) {
//        this.sucursal = sucursal;
//    }
//    
//    public void guardar() throws IOException{
//        sucursal.setId(geberarId());
//        this.guardar(sucursal);
//    }
//    public void modificar(Integer pos) throws IOException,VacioException, PosicionException{
//        this.modificar(sucursal,pos);
//    }
//     
//    public Integer generarId(){
//        return listar().size()+1;
//    }
//    
//    public boolean guardaVenta(Integer ventaPosterior, Double valor) throws EspacioException, VacioException, PosicionException{
//        if (this.sucursal != null){
//            if(ventaPosterior <= this.sucursal.getVentas().size()-1)
//                sucursal.getVentas().get(ventaPosterior).setValor(valor);
//             else 
//                throw new EspacioException();
//            
//        } else
//            throw new NullPointerException("Se debe elegir alguna sucursal");
//        
//        return true;
//    } 
//    public void generarVentasMes(){
//        for (int i= 0; i < EnumMes.values().length; i++){
//            Venta venta= new Venta();
//            venta.setId((i+1));
//            venta.setMes(EnumMes.values()[i]);
//            venta.setValor(0.0);
//            sucursal.getVentas().insertar(venta);
//        }
//    }
//}
