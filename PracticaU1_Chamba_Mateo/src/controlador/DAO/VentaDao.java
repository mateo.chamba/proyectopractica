//
//package controlador.DAO;
//
//import controlador.ed.lista.exception.PosicionException;
//import controlador.ed.lista.exception.VacioException;
//import java.io.IOException;
//import modelo.Sucursal;
//import modelo.Venta;
//
///**
// *
// * @author mateochamba
// */
//public class VentaDao extends AdaptadorDAO<Venta>{
//    private Venta venta;
//
//    public VentaDao() {
//        super(Venta .class);
//    }
//
//    public Venta getVenta() {
//        if (this.venta == null){
//            this.venta = new Venta();
//        }
//        return venta;
//    }
//
//    public void setVenta(Venta venta) {
//        this.venta = venta;
//    }
//    
//    public void guardar() throws IOException{
//        venta.setId(geberarId());
//        this.guardar(venta);
//    }
//    public void modificar(Integer pos) throws IOException, VacioException, PosicionException{
//       this.modificar(venta, pos); 
//    }
//    
//    public Integer generarId(){
//        return listar().size()+1;
//    }
//    
//    
//}
