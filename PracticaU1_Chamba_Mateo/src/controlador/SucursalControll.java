package controlador;

import controlado.Exception.EspacioException;
import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import modelo.EnumMes;
import modelo.Sucursal;
import modelo.Venta;

/**
 *
 * @author mateochamba
 */
public class SucursalControll {

    private ListaEnlazada<Sucursal> sucursales;
    private Sucursal sucursal;
    private Venta venta;
    private ListaEnlazada<Venta> lista ; // lista = ventas
   

    public SucursalControll() {
        sucursales = new ListaEnlazada<>();
        lista = new ListaEnlazada<>();
//        if (lista.estaVacio()){
//            this.lista = new ListaEnlazada<>();
//            
//            for (EnumMes mes : EnumMes.values()){
//                this.venta = new Venta();
//                getVenta().setMes(mes);
//                getVenta().setValor(0.0);
//                getVenta().setId(id);
//                id++;
//                lista.insertar(venta);
//            }
//        }
    }

    public SucursalControll(ListaEnlazada<Sucursal> sucursales, Sucursal sucursal, Venta venta, ListaEnlazada<Venta> lista) {
        this.sucursales = sucursales;
        this.sucursal = sucursal;
        this.venta = venta;
        this.lista = lista;
    }
    

   
    public ListaEnlazada<Sucursal> getSucursales() {
        return sucursales;
    }

    public void setSucursales(ListaEnlazada<Sucursal> sucursales) {
        this.sucursales = sucursales;
    }
    

    public Sucursal getSucursal() {
        return sucursal;
    }

    public void setSucursal(Sucursal sucursal) {
        this.sucursal = sucursal;
    }

    public Venta getVenta() {
        return venta;
    }

    public void setVenta(Venta venta) {
        this.venta = venta;
    }

    public ListaEnlazada<Venta> getLista() {
        return lista;
    }

    public void setLista(ListaEnlazada<Venta> lista) {
        this.lista = lista;
    }    
}

