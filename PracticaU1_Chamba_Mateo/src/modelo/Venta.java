
package modelo;

/**
 *
 * @author mateochamba
 */
public class Venta {
    private Integer id;
    private double valor;
    private EnumMes mes;



   
    public Integer getId() {
        return id;
    }

    
    public void setId(Integer id) {
        this.id = id;
    }

    
    public double getValor() {
        return valor;
    }


    public void setValor(double valor) {
        this.valor = valor;
    }


    public EnumMes getMes() {
        return mes;
    }


    public void setMes(EnumMes mes) {
        this.mes = mes;
    }
    @Override
    public String toString(){
        return mes.toString()+""+valor;
    }
    
}
