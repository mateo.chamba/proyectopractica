
package modelo;

import controlador.ed.lista.ListaEnlazada;



/**
 *
 * @author mateochamba
 */
public class Sucursal{
    private Integer id;
    private String nombre;
    private ListaEnlazada<Venta> ventas ;

    public Sucursal() {
        ventas = new ListaEnlazada<>();
    }

    public Sucursal(Integer id, String nombre, ListaEnlazada<Venta> ventas) {
        this.id = id;
        this.nombre = nombre;
        this.ventas = ventas;
    }

    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ListaEnlazada<Venta> getVentas() {
        return ventas;
    }

    public void setVentas(ListaEnlazada<Venta> ventas) {
        this.ventas = ventas;
    }
@Override
public String toString(){
    return getNombre()+""+getId();
}
    



  
    
    


}
