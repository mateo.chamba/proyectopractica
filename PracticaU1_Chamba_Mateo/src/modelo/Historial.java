
package modelo;

import java.util.Date;

/**
 *
 * @author mateochamba
 */
public class Historial {
    private Venta venta; 
    private Date fecha; 

    public Historial() {
    
    }

    public Historial(Venta venta, Date fecha) {
        this.venta = venta;
        this.fecha = fecha;
    }

    public Venta getVenta() {
        return venta;
    }

    public void setVenta(Venta venta) {
        this.venta = venta;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    
    
}
