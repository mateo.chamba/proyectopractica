package vista;



import controlador.DAO.AdaptadorDaoReclamo;
import controlador.ed.cola.Cola;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.TopeException;
import controlador.ed.lista.exception.VacioException;
import java.io.IOException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelo.Reclamo;


/**
 *
 * @author mateochamba
 */
public class FrmReclamos extends javax.swing.JDialog {

    private AdaptadorDaoReclamo<Reclamo> daoCola = new AdaptadorDaoReclamo(Cola.class);
    private Reclamo reclamo = new Reclamo();

    public FrmReclamos(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        txtAreaReclamo = new javax.swing.JTextArea();
        jToggleButton1 = new javax.swing.JToggleButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        txtAreaReclamo.setColumns(20);
        txtAreaReclamo.setRows(5);
        jScrollPane1.setViewportView(txtAreaReclamo);

        jToggleButton1.setText("Enviar");
        jToggleButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 52, Short.MAX_VALUE)
                .addComponent(jToggleButton1)
                .addGap(40, 40, 40))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(82, 82, 82)
                        .addComponent(jToggleButton1)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jToggleButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton1ActionPerformed
        try {
            guardarReclamo();
        } catch (VacioException ex) {
            Logger.getLogger(FrmReclamos.class.getName()).log(Level.SEVERE, null, ex);
        } catch (PosicionException ex) {
            Logger.getLogger(FrmReclamos.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TopeException ex) {
            Logger.getLogger(FrmReclamos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jToggleButton1ActionPerformed

   
    public static void main(String args[]) {
   

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FrmReclamos dialog = new FrmReclamos(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToggleButton jToggleButton1;
    private javax.swing.JTextArea txtAreaReclamo;
    // End of variables declaration//GEN-END:variables

    private void guardarReclamo() throws VacioException, PosicionException, TopeException {
        Cola<Reclamo> cola = (Cola<Reclamo>) daoCola.listar();
        try {
            if (cola.getCola().isFull()) {
                borrarPeticiones();
                JOptionPane.showMessageDialog(null, "Lista de peticiones llenas, intente denuevo mas tarde");
            }
            else{
                Reclamo nuevoReclamo = new Reclamo();
                nuevoReclamo.setReclamo(txtAreaReclamo.getText().toString());
                nuevoReclamo.setDate(new Date());
                cola.queue(nuevoReclamo);
                daoCola.guardarCola(cola);
                JOptionPane.showMessageDialog(null, "Reclamo registrado");
            }
        } catch (TopeException | IOException e) {
            System.err.println(e.getMessage());
            JOptionPane.showMessageDialog(null, "Lista de peticiones llenas, intente denuevo mas tarde");
        }

    }

    private void borrarPeticiones() throws VacioException, PosicionException, TopeException, IOException {
        Cola<Reclamo> cola = (Cola<Reclamo>) daoCola.listar();
        Cola<Reclamo> colaNueva = new Cola<>(10);

        while (!cola.getCola().estaVacio()) {
            Reclamo r = cola.dequeue();
            Date fechaActual = new Date();
            Date fechaReclamo = r.getDate();
            long diferenciaMillis = fechaActual.getTime() - fechaReclamo.getTime();
            long unaHoraEnMillis = 3600000;

            if (diferenciaMillis <= unaHoraEnMillis) {
                colaNueva.queue(r);
            }
        }

        daoCola.guardarCola(colaNueva);
    }
}
