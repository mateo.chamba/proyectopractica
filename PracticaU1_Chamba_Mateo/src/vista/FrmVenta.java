
package vista;

import controlador.DAO.AdaptadorDAO;
import controlador.DAO.AdaptadorDaoCola;
import controlador.SucursalControll;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.TopeException;
import controlador.ed.lista.exception.VacioException;
import controlador.ed.pila.Pila;
import java.util.Date;
import java.util.logging.Level;
import javax.swing.JOptionPane;
import modelo.Historial;
import modelo.Sucursal;
import modelo.Venta;
import vista.modelo.tabla.ModeloTablaVenta;
import vista.utilies.UtilidadesVista;

/**
 *
 * @author mateochamba
 */
public class FrmVenta extends javax.swing.JDialog {
    
    private ModeloTablaVenta modelo = new ModeloTablaVenta();
    private SucursalControll control;
    private AdaptadorDAO<Sucursal> dao = new AdaptadorDAO(Sucursal.class);
    private Historial historial = new Historial();
    private AdaptadorDaoCola<Historial> daoCola = new AdaptadorDaoCola(Historial.class);
    private int fila = -1;
    
    private Pila<String> histor = new Pila<>(5);
    UtilidadesVista utilVista = new UtilidadesVista();

    public FrmVenta(java.awt.Frame parent, boolean modal, SucursalControll sc) {
        super(parent, modal);
        initComponents();
        //if (sc.getSucursal().getVentas().estaVacio()){
        //    sc.generarVentasMes();
        //}
        this.control = sc;
        lbmSucursal.setText(sc.getSucursal().getNombre());
        cargarTabla();
        
    }    
     private void cargarTabla(){
        modelo.setDatos(control.getSucursal().getVentas());
        TblTabla.setModel(modelo);
        TblTabla.updateUI();
    }
    
    private void limpiar ( ){
        this.control.setVenta(null);
        txtValor.setText("");
        lblMes.setText("");
        fila = -1; 
        cargarTabla();
    }
    private void subirVenta() throws VacioException, PosicionException {
        fila = TblTabla.getSelectedRow();
        
        if(fila >= 0 ){
            control.setVenta(control.getSucursal().getVentas().get(fila));
            txtValor.setText(this.control.getVenta().getValor()+"");
            lblMes.setText(this.control.getVenta().getMes().toString());
        }else 
             JOptionPane.showMessageDialog(null, "Seleccionar  ventana de la tabla", "Error",JOptionPane.ERROR_MESSAGE );

    }
    private void modificar() throws VacioException,TopeException {
        if (!txtValor.getText().trim().isEmpty() && !lblMes.getText().trim().isEmpty()){
            try {
                Venta nuevaVenta = new Venta();
                nuevaVenta.setId(control.getVenta().getId());
                nuevaVenta.setMes(control.getVenta().getMes());
                nuevaVenta.setValor(Double.parseDouble(txtValor.getText()));
                control.getSucursal().getVentas().modificar(nuevaVenta, fila);
                dao.modificar(control.getSucursal(), control.getSucursal().getId());
                guardarHistorial();
                histor.push("Se ha modificado un valor en la sucursal " + control.getSucursal().getNombre());

                this.limpiar();
                JOptionPane.showMessageDialog(null, "Actualizado", "Message", JOptionPane.INFORMATION_MESSAGE);

            } catch (PosicionException ex) {
                System.out.println(ex.getMessage());
            }
        }else{
            JOptionPane.showMessageDialog(null, "Seleccionar una venta ", "Error", JOptionPane.ERROR_MESSAGE);

        }
    }
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        lbmSucursal = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtValor = new javax.swing.JTextField();
        btnGuardar = new javax.swing.JButton();
        lblMes = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TblTabla = new javax.swing.JTable();
        btnSeleccionar = new javax.swing.JButton();
        jToggleButton1 = new javax.swing.JToggleButton();
        jToggleButton2 = new javax.swing.JToggleButton();
        btnSalir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(204, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Ventas de Sucursales"));
        jPanel1.setLayout(null);

        jLabel1.setText("Nombre de la sucursal:");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(100, 70, 400, 17);

        lbmSucursal.setText("<Nombre de la sucursal>");
        jPanel1.add(lbmSucursal);
        lbmSucursal.setBounds(100, 100, 390, 17);

        jPanel2.setBackground(new java.awt.Color(153, 204, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Datos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Fira Sans", 0, 13), new java.awt.Color(0, 51, 153))); // NOI18N

        jLabel3.setText("Mes de la venta:");

        jLabel4.setText("Valor de la venta:");

        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        lblMes.setEnabled(false);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(125, 125, 125)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblMes))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtValor, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(197, 197, 197)
                        .addComponent(btnGuardar)))
                .addContainerGap(143, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(60, 60, 60)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtValor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblMes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(btnGuardar)
                .addContainerGap(9, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel2);
        jPanel2.setBounds(100, 140, 500, 180);

        jPanel3.setBackground(new java.awt.Color(102, 153, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Informacion", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Fira Sans", 0, 13), new java.awt.Color(0, 0, 153))); // NOI18N
        jPanel3.setLayout(null);

        TblTabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(TblTabla);

        jPanel3.add(jScrollPane1);
        jScrollPane1.setBounds(50, 20, 360, 180);

        btnSeleccionar.setText("Seleccionar");
        btnSeleccionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSeleccionarActionPerformed(evt);
            }
        });
        jPanel3.add(btnSeleccionar);
        btnSeleccionar.setBounds(50, 220, 100, 23);

        jToggleButton1.setText("Historial");
        jToggleButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton1ActionPerformed(evt);
            }
        });
        jPanel3.add(jToggleButton1);
        jToggleButton1.setBounds(170, 220, 110, 23);

        jToggleButton2.setText("Reclamos");
        jToggleButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton2ActionPerformed(evt);
            }
        });
        jPanel3.add(jToggleButton2);
        jToggleButton2.setBounds(290, 220, 120, 23);

        btnSalir.setText("Off");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });
        jPanel3.add(btnSalir);
        btnSalir.setBounds(430, 220, 50, 23);

        jPanel1.add(jPanel3);
        jPanel3.setBounds(100, 320, 500, 260);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 680, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 606, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 6, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        try {
            try {
                modificar();
            } catch (VacioException ex) {
                java.util.logging.Logger.getLogger(FrmVenta.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (TopeException ex) {
            java.util.logging.Logger.getLogger(FrmVenta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnSeleccionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSeleccionarActionPerformed
        try {
            subirVenta();
        } catch (VacioException ex) {
            java.util.logging.Logger.getLogger(FrmVenta.class.getName()).log(Level.SEVERE, null, ex);
        } catch (PosicionException ex) {
            java.util.logging.Logger.getLogger(FrmVenta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnSeleccionarActionPerformed

    private void jToggleButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton1ActionPerformed
    new FrmHistorial(null, true).setVisible(true);
    }//GEN-LAST:event_jToggleButton1ActionPerformed

    private void jToggleButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton2ActionPerformed
        new FrmReclamos(null, true).setVisible(true);
    }//GEN-LAST:event_jToggleButton2ActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        utilVista.guardarHistorial(histor);
        this.dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmVenta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmVenta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmVenta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmVenta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FrmVenta dialog = new FrmVenta(new javax.swing.JFrame(), true, null);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable TblTabla;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JButton btnSeleccionar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToggleButton jToggleButton1;
    private javax.swing.JToggleButton jToggleButton2;
    private javax.swing.JTextField lblMes;
    private javax.swing.JLabel lbmSucursal;
    private javax.swing.JTextField txtValor;
    // End of variables declaration//GEN-END:variables
   
    private void guardarHistorial() throws VacioException, PosicionException, TopeException{
        Historial historialNuevo = new Historial(); 
        historialNuevo.setVenta(control.getSucursal().getVentas().get(fila));
        historialNuevo.setFecha(new Date());
        
//            Cola<Historial> colaHis = colaDao.listar();
//        
//        if (colaHis.getCola().isFull())
//            colaDao.listar();
//        
//        colaDao.guardar(historialNuevo);
//    }
   }
}