
package vista;

import controlado.Exception.EspacioException;
import controlador.DAO.AdaptadorDAO;
import controlador.SucursalControll;
import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import java.util.logging.Level;
import javax.swing.JOptionPane;
import modelo.EnumMes;
import modelo.Sucursal;
import modelo.Venta;
import vista.modelo.tabla.ModeloTablaSucursal;

/**
 *
 * @author mateochamba
 */
public class FrmSucursal extends javax.swing.JDialog {
    
    private AdaptadorDAO<Sucursal> dao = new AdaptadorDAO(Sucursal.class);
    private SucursalControll control = new SucursalControll();
    private ModeloTablaSucursal modelo = new ModeloTablaSucursal();
    private int id = 0;
   
    
    public FrmSucursal(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        EmpezarVenta();
        cargarTabla();
    }
  
    
    private void limpiar(){
        //this.control.setSucursal(null);
        txtnombre.setText("");
        cargarTabla();
    }
    
    public void guardarSucursal() throws EspacioException{

        if (!txtnombre.getText().trim().isEmpty()) {
            Sucursal s = new Sucursal();

            s.setId(id);
            s.setNombre(txtnombre.getText().toString());
            s.setVentas(control.getLista());

            control.getSucursales().insertar(s);

            JOptionPane.showMessageDialog(null, "Se ha guardado la sucursal");
            id++;
            dao.guardar(s);
            limpiar();

        } else {
            JOptionPane.showMessageDialog(null, "Llenar todos los campos", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
        private void cargarTabla() {
        modelo.setDatos(dao.listar());
        TblTabla.setModel(modelo);
        TblTabla.updateUI();
        
} 

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tblTabla = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtnombre = new javax.swing.JTextField();
        btnguardar = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TblTabla = new javax.swing.JTable();
        btnVentas = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        tblTabla.setBackground(new java.awt.Color(153, 204, 255));
        tblTabla.setBorder(javax.swing.BorderFactory.createTitledBorder("SUCURSAL"));
        tblTabla.setLayout(null);

        jPanel2.setBackground(new java.awt.Color(153, 204, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel2.setForeground(new java.awt.Color(255, 0, 0));
        jPanel2.setLayout(null);

        jLabel1.setBackground(new java.awt.Color(0, 0, 0));
        jLabel1.setText("Nombre:");
        jPanel2.add(jLabel1);
        jLabel1.setBounds(20, 60, 110, 17);
        jPanel2.add(txtnombre);
        txtnombre.setBounds(140, 60, 340, 23);

        btnguardar.setText("Guardar");
        btnguardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnguardarActionPerformed(evt);
            }
        });
        jPanel2.add(btnguardar);
        btnguardar.setBounds(220, 100, 190, 23);

        tblTabla.add(jPanel2);
        jPanel2.setBounds(20, 70, 540, 140);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Lista de Sucursales"));

        TblTabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(TblTabla);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 518, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 304, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(129, 129, 129))
        );

        tblTabla.add(jPanel3);
        jPanel3.setBounds(20, 220, 540, 340);

        btnVentas.setText("Ventas");
        btnVentas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVentasActionPerformed(evt);
            }
        });
        tblTabla.add(btnVentas);
        btnVentas.setBounds(230, 620, 110, 23);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tblTabla, javax.swing.GroupLayout.DEFAULT_SIZE, 591, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tblTabla, javax.swing.GroupLayout.DEFAULT_SIZE, 681, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnguardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnguardarActionPerformed
        try {
            guardarSucursal();
        } catch (EspacioException ex){
            java.util.logging.Logger.getLogger(FrmSucursal.class.getName()).log(Level.SEVERE, null, ex);

        }
    }//GEN-LAST:event_btnguardarActionPerformed

    private void btnVentasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVentasActionPerformed
//        try {
//            int fila = TblTabla.getSelectedRow();
//            if (fila >= 0) {
//                if (modelo.getDatos().get(fila) != null) {
//                    this.control.setSucursal(modelo.getDatos().get(fila));
//                    new FrmVenta(null, true, this.control).setVisible(true);
//                    this.control.modificar(fila);
//                    cargarTabla();
//                    TblTabla.clearSelection();
//
//                } else {
//                    JOptionPane.showMessageDialog(null, "Ingresar una sucursal", "Error", JOptionPane.ERROR_MESSAGE);
//                }
//            }
//        } catch (Exception e) {
//        }
try {
            subirVenta();
        } catch (VacioException ex) {
            java.util.logging.Logger.getLogger(FrmSucursal.class.getName()).log(Level.SEVERE, null, ex);

        } catch (PosicionException ex) {
            java.util.logging.Logger.getLogger(FrmSucursal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnVentasActionPerformed

    
   
    
    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FrmSucursal dialog = new FrmSucursal(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable TblTabla;
    private javax.swing.JButton btnVentas;
    private javax.swing.JButton btnguardar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel tblTabla;
    private javax.swing.JTextField txtnombre;
    // End of variables declaration//GEN-END:variables
    private void EmpezarVenta(){
        int ventaId = 0;
        if (control.getLista().estaVacio()){
            control.setLista(new ListaEnlazada<>());
            
            for (EnumMes mes : EnumMes.values()){
                Venta vent = new Venta();
                vent.setMes(mes);
                vent.setValor(0.0);
                vent.setId(ventaId);
                ventaId++;
                control.getLista().insertar(vent);
            }
        }
    }
    private void subirVenta()throws VacioException, PosicionException{
        int fila = TblTabla.getSelectedRow();
        if (fila >= 0){
            if (dao.listar().get(fila)!= null){
                control.setSucursal(dao.listar().get(fila));
                new FrmVenta(null, true, control).setVisible(true);
                cargarTabla();
                TblTabla.clearSelection();
            }else{
                JOptionPane.showMessageDialog(null, "Ingresar la sucursal", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }else{
            JOptionPane.showMessageDialog(null, "Seleccione una sucursal de la tabla", "Error", JOptionPane.ERROR_MESSAGE);

        }
    }
}
