
package vista.modelo.tabla;

import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import javax.swing.table.AbstractTableModel;
import modelo.Venta;

/**
 *
 * @author mateochamba
 */
    public class ModeloTablaVenta extends AbstractTableModel{
        private ListaEnlazada<Venta> datos = new ListaEnlazada<>();
        

        
        
    @Override
    public int getRowCount() {
        return getDatos().size();
    }

    

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt(int i, int i1) {
        
            
            
            try {
                Venta s = getDatos().get(i);
                 switch (i1){
                case 0:
                    return (s != null) ? s.getMes().toString() :"NO DEFINIDO ";
                case 1:
                    return (s != null) ? s.getValor(): 0.0;
                    
                    default:
                        return null;
            } 
            
        } catch (VacioException ex) {
                System.out.println(ex.getMessage());
        } catch (PosicionException ex){
                System.out.println(ex.getMessage());
        }
           return null;
    }
    
    @Override
    public String getColumnName (int column){
                switch (column){
                    case 0:
                        return "Mes";
                    case 1: 
                        return "Valor";
                    default:
                        return null;
                }
            }
    public ListaEnlazada<Venta> getDatos() {
        return datos;
    }

    public void setDatos(ListaEnlazada<Venta> datos) {
        this.datos = datos;
    }        
            
    
}
