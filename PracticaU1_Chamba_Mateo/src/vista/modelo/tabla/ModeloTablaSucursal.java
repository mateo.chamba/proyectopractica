
package vista.modelo.tabla;

import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import controlador.util.Utilidades;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.AbstractTableModel;
import modelo.Sucursal;

/**
 *
 * @author mateochamba
 */
public class ModeloTablaSucursal extends AbstractTableModel {

    private ListaEnlazada<Sucursal> datos = new ListaEnlazada<>();

    @Override
    public int getRowCount() {
        return datos.size();

    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int i, int i1) {

        Sucursal s;

        try {
            s = getDatos().get(i);
            switch (i1) {
                case 0:
                    return (s != null) ? s.getNombre() : "EL VALOR NO ESTA DEFINIDO";
                case 1:
                    return (s != null) ? Utilidades.sumarVenta(s) : 0.0;
                case 2:
                    return (s != null) ? Utilidades.mediaVentas(s) : 0.0;

                default:
                    return null;
            }
        } catch (VacioException ex) {
            Logger.getLogger(ModeloTablaSucursal.class.getName()).log(Level.SEVERE, null, ex);

        } catch(PosicionException ex){
            Logger.getLogger(ModeloTablaSucursal.class.getName()).log(Level.SEVERE, null, ex);

        }
        return null;
    }

    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Sucursal";
            case 1:
                return "Venta al año";
            case 2:
                return "Promedio de venta";
            default:
                return null;
        }
    }

    public void setDatos(ListaEnlazada<Sucursal> datos) {
        this.datos = datos;

    }

    public ListaEnlazada<Sucursal> getDatos() {
        return datos;
    }

}
