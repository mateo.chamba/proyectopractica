
package vista.modelo.tabla;

import controlador.ed.cola.Cola;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import javax.swing.table.AbstractTableModel;
import modelo.Historial;

/**
 *
 * @author mateochamba
 */
public class ModeloTablaHstorial extends AbstractTableModel{
    private Cola<Historial> dato = new Cola<>(10);
    
    public ModeloTablaHstorial(){
    
    }
    
    
    @Override
    public int getRowCount() {
        return dato.getCola().size();
    }

    @Override
    public int getColumnCount() {
    return 3;     
    }

    @Override
    public Object getValueAt(int i, int i1) {
        try {
            Historial his = dato.getCola().get(i);
            switch (i1){
                case 0: 
                    return (his != null) ? his.getFecha().getDate(): "No definido";
                case 1: 
                    return (his != null) ? his.getVenta().getMes().name(): "no definido";
                case 2: 
                    return (his != null) ? his.getVenta().getValor(): "no definido";
                default:return null;
            }
        } catch (VacioException ex) {
            System.out.println(ex.getMessage());
        } catch (PosicionException ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }
     @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0: 
                return "Fecha de cambio";
            case 1: 
                return "Mes de la venta";
            case 2: 
                return "Valor cambiado";
            default:
                return null;
        }
    }
    public Cola<Historial> getDato() {
        return dato;
    }

    public void setDato(Cola<Historial> dato) {
        this.dato = dato;
    }
    
    
}
