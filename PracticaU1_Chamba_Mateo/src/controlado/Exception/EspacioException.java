
package controlado.Exception;

/**
 *
 * @author mateochamba
 */
public class EspacioException extends Exception{
    
    public EspacioException(String mensaje){
        super(mensaje);
    }
    
    public EspacioException (){
        super("Espacion lleno");
    }
   
}
